extends Node

func _on_effect_prepared(effect: UnprocessedEffect) -> void:
	effect.write_trait('damage', { modifier = 2 })

func _on_effect_applied(effect: ProcessedEffect):
	var damage := effect.read_trait('damage')
	var rolls := []
	var total := damage.modifier as int
	for _i in damage.dice_num:
		var roll := randi() % damage.dice_size as int + 1
		total += roll
		rolls.append(roll)
	effect.write_trait('roll_result', { total = total, rolls = rolls })

func _on_effect_chained(effect: ResolvedEffect, buffer: Array) -> void:
	var roll_result := effect.read_trait('roll_result')
	for roll in roll_result.rolls:
		if roll == 6:
			var new_effect := UnprocessedEffect.new()
			new_effect.write_trait('critical', {})
			buffer.append(new_effect)
			return

func _on_button_pressed() -> void:
	var effect := UnprocessedEffect.new()
	effect.write_trait('damage', { dice_num = 2, dice_size = 6, modifier = 0 })
	RuleServer.push_effect(effect)


