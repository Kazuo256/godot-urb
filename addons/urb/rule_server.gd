extends Node

onready var _effect_queue := []
onready var _effect_log := []

func _physics_process(delta):
	var buffer := []
	var flags := SceneTree.GROUP_CALL_REALTIME
	var group := Rule.RULE_GROUP
	var tree := get_tree()
	while not _effect_queue.empty():
		var effect := _effect_queue.pop_front() as UnprocessedEffect
		tree.call_group_flags(flags, group, 'prepare_effect', effect)
		var processed_effect := effect.processed()
		tree.call_group_flags(flags, group, 'apply_effect', processed_effect)
		var resolved_effect := processed_effect.resolved()
		tree.call_group_flags(flags, group, 'chain_effect', resolved_effect, buffer)
		_effect_log.append(resolved_effect)
	_effect_queue.append_array(buffer)

func push_effect(effect: UnprocessedEffect):
	_effect_queue.push_back(effect)

func get_log_size() -> int:
	return _effect_log.size()

func get_logged_effect(i: int) -> ResolvedEffect:
	return _effect_log[-1 - i]

func clear_log():
	_effect_log.clear()
