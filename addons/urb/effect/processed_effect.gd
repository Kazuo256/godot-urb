class_name ProcessedEffect extends Effect

func _init(traits: Dictionary):
	_traits = traits.duplicate()
	_mark_all(PROCESSED_FLAG)

func write_trait(trait_name: String, fields: Dictionary):
	var trait := _traits.get(trait_name, {}) as Dictionary
	assert(
		not _is_marked(trait, PROCESSED_FLAG),
		"tried to change an effect trait that was already processed."
	)
	trait.merge(fields, true)
	_traits[trait_name] = trait

func resolved() -> ResolvedEffect:
	return ResolvedEffect.new(_traits)
