class_name Effect extends Resource

const PROCESSED_FLAG = '_processed'
const RESOLVED_FLAG = '_resolved'

export var _traits: Dictionary

func has_trait(trait_name: String) -> bool:
	return _traits.has(trait_name)

func read_trait(trait_name: String) -> Dictionary:
	var trait = _traits.get(trait_name)
	return trait.duplicate() if trait != null else null

func _mark_all(flag: String):
	for trait in _traits.values():
		trait[flag] = true

func _is_marked(trait: Dictionary, flag: String) -> bool:
	return trait.get(flag)

func _to_string() -> String:
	var text := 'effect\n'
	for trait_name in _traits:
		text += "\t%s\n" % trait_name
		var trait := _traits[trait_name] as Dictionary
		for key in trait:
			var field_name := key as String
			if not field_name.begins_with('_'):
				text += "\t\t%s: %s\n" % [field_name, trait[key]]
	return text + '\n'
