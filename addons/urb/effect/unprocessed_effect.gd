class_name UnprocessedEffect extends Effect

func _init():
	_traits = {}

func write_trait(trait_name: String, fields: Dictionary):
	var trait := _traits.get(trait_name, {}) as Dictionary
	trait.merge(fields, true)
	_traits[trait_name] = trait

func processed() -> ProcessedEffect:
	return ProcessedEffect.new(_traits)
