class_name ResolvedEffect extends Effect

func _init(traits: Dictionary):
	_traits = traits.duplicate()
	_mark_all(RESOLVED_FLAG)
