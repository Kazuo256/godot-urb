tool
extends EditorPlugin

const URB_PATH := 'res://addons/urb/rule_server.gd'

func _enter_tree():
	add_autoload_singleton('RuleServer', URB_PATH)

func _exit_tree():
	remove_autoload_singleton('RuleServer')
