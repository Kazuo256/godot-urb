class_name WithTrait extends Predicate

export var trait_name := ''

func _accepts(effect: Effect) -> bool:
	return effect.has_trait(trait_name)
