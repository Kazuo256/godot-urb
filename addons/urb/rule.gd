class_name Rule extends Node

signal effect_prepared(effect)
signal effect_applied(effect)
signal effect_chained(effect, buffer)

const RULE_GROUP := 'urb/rule'

func _ready():
	add_to_group(RULE_GROUP)

func matches(effect: Effect) -> bool:
	for predicate in get_children():
		if predicate is Predicate:
			if not predicate._accepts(effect):
				return false
	return true

func prepare_effect(effect: UnprocessedEffect):
	if matches(effect):
		emit_signal('effect_prepared', effect)

func apply_effect(effect: ProcessedEffect):
	if matches(effect):
		emit_signal('effect_applied', effect)

func chain_effect(effect: ResolvedEffect, buffer: Array):
	if matches(effect):
		emit_signal('effect_chained', effect, buffer)
